const request = require('supertest');
const app = require('./app');

describe('Authentication tests', () => {
  test('Successful login', async () => {
    const response = await request(app)
      .post('/login')
      .send({ username: 'user123', password: 'Password123' });
    expect(response.statusCode).toBe(200);
  });

  test('Failed login', async () => {
    const response = await request(app)
      .post('/login')
      .send({ username: 'user123', password: 'somepw' });
    expect(response.statusCode).toBe(401);
  });
});
